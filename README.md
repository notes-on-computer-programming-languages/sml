# sml

Standard ML [Standard_ML](https://en.wikipedia.org/wiki/Standard_ML)

[[_TOC_]]

# Documentation
# Features
## Lazy evaluation
* [*Lazy*](http://mlton.org/Lazy)
* [*Lazy evaluation*
  ](https://riptutorial.com/sml/example/23626/lazy-evaluation)

## OOP
* [*ObjectOrientedProgramming*](http://mlton.org/ObjectOrientedProgramming)

# OCaml and SML
* [*OCaml and SML*](http://mlton.org/OCaml)
  * [*Standard ML and Objective Caml, Side by Side*
    ](https://people.mpi-sws.org/~rossberg/sml-vs-ocaml.html)
  * [*Comparing Objective Caml and Standard ML*
    ](http://adam.chlipala.net/mlcomp/)
* [*Should I use Standard ML or OCaml? What are the main differences? Which is easier and more commonly used?*
  ](https://www.quora.com/Should-I-use-Standard-ML-or-OCaml-What-are-the-main-differences-Which-is-easier-and-more-commonly-used)
  2017
* [*What are the key differences between Standard ML and OCaml?*
  ](https://www.quora.com/What-are-the-key-differences-between-Standard-ML-and-OCaml)

# Critiques
* [*Why isn't Standard ML more popular?*
  ](https://www.quora.com/Why-isnt-Standard-ML-more-popular)
  2017
* [*What are the best use cases for Standard ML?*
  ](https://www.quora.com/What-are-the-best-use-cases-for-Standard-ML)
  2016
* [*Why not program in Standard ML?*
  ](https://tech.labs.oliverwyman.com/blog/2014/10/08/why-not-program-in-standard-ml/)
  2014
* [*What is it like to use Standard ML in production?*
  ](https://www.quora.com/What-is-it-like-to-use-Standard-ML-in-production)
  2014
